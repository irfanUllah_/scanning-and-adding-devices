#!/bin/bash
# pre-req1/2: - volttron virtualenv activated
# pre-req2/2: - must be in folder volttron/scripts/bacnet
directory="devices_list$EPOCHREALTIME"
echo "Creating Directory ${directory}"
mkdir $directory
echo "Done.. "
echo "Scanning all devices and saving the list to $directory/devices.list"
python3 bacnet_scan.py > "$directory/devices.list"
echo "Done.."
echo "Storing device Ids to $directory/device.Ids"
echo "Doing some processing. "
cat "$directory/devices.list" | grep Id > "$directory/device.Ids"
grep -o '[[:digit:]]*' $directory/device.Ids > $directory/Identifiers
mapfile  myArray < $directory/Identifiers
echo "Saving Configuration for each device.."
for t in ${myArray[@]}; 
do 
python3 grab_bacnet_config.py "${t//[$'\t\r\n ']}" --driver-out "$directory/${t//[$'\t\r\n ']}.config" --registry-out "$directory/${t//[$'\t\r\n ']}.registry" ;
echo "$directory/${t//[$'\t\r\n ']}.config"
value=$(<"$directory/${t//[$'\t\r\n ']}.config")
newvalue=$value
newvalue2="${newvalue::-2}"
newvalue2+=',
    "interval":60 
}'

echo "$newvalue2">"$directory/${t//[$'\t\r\n ']}.config"
 done

for t in ${myArray[@]}; 
do 
echo "Storing driver  configuration for ${t//[$'\t\r\n ']}.."
volttron-ctl config store platform.driver "devices/my_campus/my_building/${t//[$'\t\r\n ']}" "$directory/${t//[$'\t\r\n ']}.config";
echo "Done.. "
echo "Storing registry  configuration for ${t//[$'\t\r\n ']}.."
volttron-ctl config store platform.driver "registry_configs/${t//[$'\t\r\n ']}.registry" "$directory/${t//[$'\t\r\n ']}.registry" --csv;
echo "Done.. "
 done
# for volttron-ctl config store platform.driver devices/my_campus/my_building/hvac1 modbus1.config
echo "Configurations have been loaded to the volttron config store.."
echo "List of configurations.."
vctl config list platform.driver

# vctl config store 

